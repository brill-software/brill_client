// © 2022 Brill Software Limited - Brill HTML Components, distributed under the MIT License.
import React, {Component} from "react"
import { Theme } from "lib/ComponentLibraries/material_ui/theme/Theme"
import withStyles from "@mui/styles/withStyles"
import Router from "lib/Router/Router"

/**
 * HTML A (anchor) component for external links.
 */

interface Props {
    theme: Theme
    classes: any
    children: any
    href?: string
    route?: string
    target?: string
    [propName: string]: any
}

interface State {
}

class A extends Component<Props, State> {

    onClickHandler(event: any) {
        if (this.props.route?.toLowerCase() === "back") {
            Router.goBackToPreviousPage()
        } else {
            Router.goToPage(this.props.route)
        }
    }

    render() {
        const {id, theme, classes, children, href, target, ...other} = this.props

        if (this.props.route) {
            return (
                <a className={classes.root} onClick={event => this.onClickHandler(event)} {...other}>{children}</a>
            )
        }

        return (
            <a className={classes.root} href={href} target={target} {...other}>{children}</a>
        )
    }

    static defaultStyles(theme: Theme): any {
        return  {
            root: {
                curor: "none",
                '&:hover': {
                    cursor: "pointer",
                    boxShadow: "5px 10px 18px"
                },
                ...theme.components?.A?.styleOverrides?.root }
            }
    }

}

export default withStyles(A.defaultStyles, {withTheme: true})(A)