// © 2021 Brill Software Limited - Brill MUI Components, distributed under the MIT License.
import React, {Component} from "react"
import { Button as MuiButton } from "@mui/material"
import { Theme } from "lib/ComponentLibraries/material_ui/theme/Theme"
import Router from "lib/Router/Router"
import { IconUtils } from "lib/utils/IconUtils"
import withStyles from "@mui/styles/withStyles"
import { MB } from "lib/MessageBroker/MB"

/**
 * Link Button component.
 * 
 */

interface Props {
    theme: Theme
    classes: any
    startIcon?: string
    title: string
    endIcon?: string
    publishToTopic?: string
    actionObj?: string
    route?: string
    [propName: string]: any
}

interface State {
}

class LinkButton extends Component<Props, State> {

    static defaultStyles(theme: Theme): any {
        return  { root: { ...theme.components?.LinkButton?.styleOverrides?.root }}
    }

    onClickHandler() {
        MB.publish(this.props.publishToTopic, this.props.actionObj)
        if (this.props.route) {
            if (this.props.route?.toLowerCase() === "back") {
                Router.goBackToPreviousPage()
            } else {
                Router.goToPage(this.props.route)
            }   
        }
    }

    render() {
        const {theme, classes, startIcon, title, endIcon, ...other} = this.props
        const startIconAttr = {startIcon: IconUtils.resolveIcon(startIcon)}
        const endIconAttr = {endIcon: IconUtils.resolveIcon(endIcon)}

        return (
            <MuiButton 
                className={classes.root}
                {...startIconAttr}
                {...endIconAttr}
                onClick={() => this.onClickHandler()}
                {...other}
            >{title}</MuiButton>
        )
    }
}

export default withStyles(LinkButton.defaultStyles, { withTheme: true})(LinkButton)