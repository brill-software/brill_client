// © 2024 Brill Software Limited - Brill MUI Components, distributed under the MIT License.
import React, { Component } from "react"
import Tooltip from "@mui/material/Tooltip"
import { IconButton as MuiIconButton } from '@mui/material'
import { Theme } from "lib/ComponentLibraries/material_ui/theme/Theme"
import Brightness7 from "@mui/icons-material/Brightness7"
import Brightness4 from "@mui/icons-material/Brightness4"
import { MB, Token } from "lib/MessageBroker/MB"
import { THEME_MODE_TOPIC, LIGHT_MODE, DARK_MODE, getModeFromLocalStorage } from "lib/ComponentLibraries/material_ui/theme/ThemeProvider"
import withStyles from "@mui/styles/withStyles"

/**
 * Button for switching between the light and dark themes.
 * 
 */

interface Props {
    theme: Theme
    classes: any
    setDarkToolTip: string
    setLightToolTip: string
    [propName: string]: any
}

interface State {
    mode: string | undefined
}

class ThemeSwitchButton extends Component<Props, State> {
    token: Token

    constructor(props: Props) {
        super(props);
        this.state = { mode: undefined };
    }

    componentDidMount() {
        this.setState({ mode: getModeFromLocalStorage() })
    }

    componentWillUnmount() {
        MB.unsubscribe(this.token)
    }

    onClickHandler(newMode: string) {
        MB.publish(THEME_MODE_TOPIC, newMode)
        this.setState({ mode: newMode })
    }

    render() {
        const { theme, classes, setDarkToolTip, setLightToolTip, ...other } = this.props
        const { mode } = this.state

        if (mode === LIGHT_MODE) {
            return (
                <Tooltip title={setDarkToolTip}>
                    <MuiIconButton className={classes.root} aria-label={setDarkToolTip} {...other}
                        onClick={() => this.onClickHandler(DARK_MODE)}>
                        <Brightness4 />
                    </MuiIconButton>
                </Tooltip>
            )
        }

        if (mode === DARK_MODE) {
            return (
                <Tooltip title={setLightToolTip}>
                    <MuiIconButton className={classes.root} aria-label={setLightToolTip} {...other}
                        onClick={() => this.onClickHandler(LIGHT_MODE)}>
                        <Brightness7 />
                    </MuiIconButton>
                </Tooltip>
            )
        }

        return null
    }

    static defaultStyles(theme: Theme): any {
        return { root: { ...theme.components?.ThemeSwitchButton?.styleOverrides?.root } }
    }
}

export default withStyles(ThemeSwitchButton.defaultStyles, { withTheme: true })(ThemeSwitchButton)