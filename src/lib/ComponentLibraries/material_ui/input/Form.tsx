// © 2021 Brill Software Limited - Brill MUI Components, distributed under the MIT License.
import React, {Component} from "react"
import { Theme } from "lib/ComponentLibraries/material_ui/theme/Theme"
import withStyles from "@mui/styles/withStyles"
import { MB } from "lib/MessageBroker/MB"

/**
 * Form component.
 * 
 * Used to enclose text fields, checkboexes etc of a form. The publishToTopic is used to delete the form topic 
 * when the form mounts. This ensures that any previous form values are cleared.
 * 
 */

 export interface FormOverrides {
    root: object
}

interface Props {
    id: string
    classes: any
    children: any
    publishToTopic?: string
    [propName: string]: any
}

interface State {
    clearFormData: boolean
}

class Form extends Component<Props, State> {

    constructor(props: Props) {
        super(props)
        if (this.props.publishToTopic) {
            MB.delete(this.props.publishToTopic)
        }
    }
   
    render() {
        const {id, theme, classes, publishToTopic, ...other} = this.props
        
        return (     
            <form className={classes.root} noValidate autoComplete="off" {...other}>
                {this.props.children}
            </form>
        )
    }

    static defaultStyles(theme: Theme): any {
        return  {root: {...theme.components?.Form?.styleOverrides?.root}}
    } 
}

export default withStyles(Form.defaultStyles)(Form)