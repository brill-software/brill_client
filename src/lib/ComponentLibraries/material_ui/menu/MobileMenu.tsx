// © 2024 Brill Software Limited - Brill MUI Components, distributed under the MIT License.
import React, { Component } from "react"
import { Theme } from "../theme/Theme"
import withStyles from "@mui/styles/withStyles"
import MenuIcon from "@mui/icons-material/Menu"
import { IconButton as MuiIconButton, Menu as MuiMenu, MenuItem as MuiMenuItem } from "@mui/material"
import Router from "lib/Router/Router"

/**
 * Mobile Menu component. 
 * 
 */

interface MenuItem {
    label: string
    route: string
}

interface Props {
    theme: Theme
    classes: any
    menuItems: MenuItem[]
    [propName: string]: any
}
interface State {
    anchorEl: EventTarget & Element | null
    open: boolean
}

class MobileMenu extends Component<Props, State> {

    constructor(props: Props) {
        super(props)
        this.state = { anchorEl: null, open: false }
    }

    onClickHandler(event: React.MouseEvent) {
        this.setState({ open: true, anchorEl: event.currentTarget })
    }

    onCloseHandler(event: React.MouseEvent) {
        this.setState({ open: false })
    }

    onMenuItemClickHandler(event: React.MouseEvent, route: string) {
        this.setState({ open: false })
        Router.goToPage(route)
    }

    createMenuItems(): React.DOMElement<any, any>[] {
        let reactElements: Array<React.DOMElement<any, any>> = new Array<React.DOMElement<any, any>>()
        for (const item of this.props.menuItems) {
            const menuItem: any = (
                <MuiMenuItem key={item.label}
                    onClick={(event: React.MouseEvent) => { this.onMenuItemClickHandler(event, item.route) }}>
                    {item.label}
                </MuiMenuItem>
            )
            reactElements.push(menuItem)
        }
        return reactElements
    }

    render() {
        const { theme, classes, color, menuItems, ...other } = this.props
        const { open, anchorEl } = this.state
       
        const iconSxObj = {
            fontSize: "16px",
            color: "grey",
            ...theme?.components?.MobileMenu?.styleOverrides?.icon
        }
            
        return (
            <div className={classes.root} {...other}>
                <MuiIconButton  onClick={(event) => this.onClickHandler(event)}>
                    <MenuIcon sx={iconSxObj} />
                </MuiIconButton>
                <MuiMenu        
                    anchorEl={anchorEl}
                    keepMounted
                    open={open}
                    onClose={(event: any) => this.onCloseHandler(event)}
                >
                    {
                        this.createMenuItems()
                    }
                </MuiMenu>
            </div>
        )
    }

    static defaultStyles(theme: Theme): any {
        return { root: {
                    borderWidth: "3px",
                    borderStyle: "solid",
                    borderRadius: "10px",
                    borderColor: "purple",
                    display: "inline-block",
                    ...theme.components?.MobileMenu?.styleOverrides?.root 
                }
                // icon is applied using sx attribute.
        }
    }
}

export default withStyles(MobileMenu.defaultStyles, { withTheme: true })(MobileMenu)