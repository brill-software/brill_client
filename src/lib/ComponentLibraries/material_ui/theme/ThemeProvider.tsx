// © 2021 Brill Software Limited - Brill MUI Components, distributed under the MIT License.
import React, {Component} from "react"
import { ThemeProvider as MuiThemeProvider, createTheme } from "@mui/material/styles"
import { MB, Token } from "lib/MessageBroker/MB"
import { Theme } from "lib/ComponentLibraries/material_ui/theme/Theme"
import { ErrorMsg } from "lib/MessageBroker/ErrorMsg"
import { Paper } from "@mui/material"
import { LocalStorage } from "lib/utils/LocalStorage"

/**
 * Theme Provider.
 * 
 * Supports either one theme or two themes (one light, one dark). For two themes, the prop themeTopicDark
 * must be supplied to specify the dark theme. The theme can be switched by publishing a string of "light"
 * or "dark to the topic "theme_provider.mode".
 * 
 * The current theme is saved to local storage so that the choice is remembered for when the user visits
 * again.
 * 
 */
interface Props {
    id: string
    children: any
    themeTopic: string
    themeTopicDark?: string
    [propName: string]: any
}

interface State {
    theme: Theme | undefined
}

export const LIGHT_MODE = "light"
export const DARK_MODE = "dark"
export const THEME_MODE_TOPIC = "theme_provider.mode"

export function getModeFromLocalStorage() : string {
    const mode = LocalStorage.getValue(THEME_MODE_KEY)
    return (mode.length > 0 ? mode : THEME_DEFAULT_MODE) 
}

const THEME_MODE_KEY = "theme_provider_mode"
const THEME_DEFAULT_MODE = LIGHT_MODE
export default class ThemeProvider extends Component<Props, State> {
    mode: string
    tokens: Token[] = []
    themeLight: Theme | undefined
    themeDark: Theme | undefined

    constructor(props: Props) {
        super(props);
        this.state = {theme: undefined};
    }

    componentDidMount() {
        this.mode = getModeFromLocalStorage()
        this.tokens.push( MB.subscribe(this.props.themeTopic, 
            (topic, data) => this.themeLightLoadedCallback(topic, data), 
            (topic, error) => this.errorCallback(topic, error)))
        if (this.props.themeTopicDark) {
            this.tokens.push(MB.subscribe(this.props.themeTopicDark, 
                (topic, data) => this.themeDarkLoadedCallback(topic, data), 
                (topic,error) => this.errorCallback(topic, error)))
        } else {
            this.mode = LIGHT_MODE // Default to light mode if there's no dark theme.
        }
        this.tokens.push(MB.subscribe(THEME_MODE_TOPIC, 
            (topic, data)  => this.switchCallback(topic, data), 
            (topic, error) => this.errorCallback(topic, error)))
    }

    componentWillUnmount() {
        if (this.mode === DARK_MODE && this.themeDark) {
            this.unsetBodyAndHtmlTagStyles(this.themeDark)
        }
        if (this.mode === LIGHT_MODE && this.themeLight) {
            this.unsetBodyAndHtmlTagStyles(this.themeLight)
        }
        MB.unsubscribeAll(this.tokens)
    }

    themeLightLoadedCallback(topic: string, data: Theme) {
        this.themeLight = createTheme(data)
        if (this.mode === LIGHT_MODE) {
            this.setBodyAndHtmlTagStyles(this.themeLight)
            this.setState({theme: this.themeLight})
        }
    }

    themeDarkLoadedCallback(topic: string, data: Theme) {
        this.themeDark = createTheme(data)
        if (this.mode === DARK_MODE) {
            this.setBodyAndHtmlTagStyles(this.themeDark)
            this.setState({theme: this.themeDark})
        }
    }

    private setBodyAndHtmlTagStyles(theme: Theme) {
        if (theme.typography.html?.style) {
            for (const styleKey in theme.typography.html.style) {
                (document.documentElement.style as any)[styleKey] = (theme.typography.html.style as any)[styleKey]
            }
        }
        if (theme.typography.body?.style) {
            for (const styleKey in theme.typography.body.style) {
                (document.body.style as any)[styleKey] = (theme.typography.body.style as any)[styleKey]
            }
        }
    }

    private unsetBodyAndHtmlTagStyles(theme: Theme) {
        if (theme?.typography.html?.style) {
            for (const styleKey in theme.typography.html.style) { 
                (document.documentElement.style as any)[styleKey] = null
            }
        }
        if (theme?.typography.body?.style) {
            for (const styleKey in theme.typography.body.style) { 
                if (styleKey !== "margin") { // Don't unload margin as default margin of <body> is 8px.
                    (document.body.style as any)[styleKey] = null
                } 
            }
        }
    }

    errorCallback(topic: string, error: ErrorMsg) {
        console.error("Error: " + topic + " : "  + error.title + " " + error.detail + " for component " + this.props.id)
    }

    /**
     * Callback that switches the theme between light and dark.
     * 
     */
    switchCallback(topic: string, newMode: string) {
        if (this.mode === LIGHT_MODE && newMode === DARK_MODE && this.themeLight && this.themeDark) {
            // Switch to dark theme
            this.mode = DARK_MODE
            this.saveTheme()
            this.unsetBodyAndHtmlTagStyles(this.themeLight)
            this.setBodyAndHtmlTagStyles(this.themeDark)
            this.setState({theme: this.themeDark})
            return
        }

        if (this.mode === DARK_MODE && newMode === LIGHT_MODE && this.themeLight && this.themeDark) {
            // Switch to light theme
            this.mode = LIGHT_MODE
            this.saveTheme()
            this.unsetBodyAndHtmlTagStyles(this.themeDark)
            this.setBodyAndHtmlTagStyles(this.themeLight)
            this.setState({theme: this.themeLight})
        }
    }

    /**
     * Saves the current theme to local storage.
     */
    private saveTheme() {
        LocalStorage.setValue(THEME_MODE_KEY, this.mode)
    }

    render() {
        if (this.state.theme === undefined) {
            return null
        }

        const {themeTopic, themeTopicDark, ...other} = this.props

        return (
            <MuiThemeProvider theme={this.state.theme}>
                <Paper {...other}>
                    {this.props.children}
                </Paper>         
            </MuiThemeProvider>
        )
    }
}