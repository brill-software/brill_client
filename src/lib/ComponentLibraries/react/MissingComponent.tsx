// © 2021 Brill Software Limited - Brill HTML Components, distributed under the MIT License.
import { ErrorMsg } from "lib/MessageBroker/ErrorMsg"
import { MB } from "lib/MessageBroker/MB"
import React, {Component} from "react"

/**
 * MissingComponent Component
 * 
 * Inserted as a placeholder when a module can't be loaded due to missing code.
 */
interface Props {
    id: string
    [propName: string]: any
}

interface State {
}

export default class MissingComponent extends Component<Props, State> {

    componentDidMount() {
        MB.publish("app:errors:", new ErrorMsg("Page Error", `Unable to load module for component with id '${this.props.id}'.<br/>` +
            "A later software release containing the missing module will fix this issue."))
    }

    render() {
        return <div style={{border: "solid",color: "red"}}>{`Placeholder for missing component with id '${this.props.id}'.`}</div>
    }
}