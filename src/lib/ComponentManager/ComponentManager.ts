// © 2021 Brill Software Limited - Brill Framework, distributed under the MIT License.
import { ErrorUtils } from "lib/utils/ErrorUtils"
import Module from "module"

/**
 * Loads a component.
 * 
 */

export class ComponentManager {
    private static componentMap: Map<string, Module> = new Map<string, Module>()

    static async loadComponent(moduleName: string): Promise<string> {
        // Return the component if it's already loaded
        let module = ComponentManager.componentMap.get(moduleName)
        if (module !== undefined) {
            return "ok"
        }

        let newModule: any = undefined
        try {
            newModule = await import(`lib/ComponentLibraries/${moduleName}`)
        } catch (error) {
            console.log("Unable to load module: ", ErrorUtils.cvt(error).message)
            // Load a dummy component in its place.
            newModule = await import(`lib/ComponentLibraries/react/MissingComponent`)
        }        
        ComponentManager.componentMap.set(moduleName, newModule)
        return "ok"     
    }

    static getAlreadyLoadedComponent(moduleName: string): Module | undefined{
        let module: Module | undefined = ComponentManager.componentMap.get(moduleName)
        if (module === undefined) {
            console.error(`Module ${moduleName} is undefined`)
        }
        return module
    }
}