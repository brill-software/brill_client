// © 2021 Brill Software Limited - Brill Framework, distributed under the MIT License.
export class Site {
    appDescrip: string
    loginPage: string
    homePage: string
    notFoundPage: string
}
