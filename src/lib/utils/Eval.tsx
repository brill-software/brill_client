// © 2021 Brill Software Limited - Brill Utils, distributed under the MIT License.
/* eslint no-new-func: 0 */

import { MB } from "lib/MessageBroker/MB"
import { ErrorUtils } from "./ErrorUtils"

/**
 * Executes JavaScript validation code in a sandbox.
 * 
 */



export class Eval {

    static hasMoreThanOneAt(value: string): boolean {
        let atCount: number = 0
        for (let i = 0; i < value.length; i++) {
            if (value.charAt(i) === '@') {
                atCount++
            }
        }
        return atCount > 1
    }

    static isProduction(): boolean {
        return (process.env.NODE_ENV === "production")
    }

    // The permissions are set by the login button. 
    private static userPermissions: string = ""

    static setUserPermissions(permissions: string) {
        Eval.userPermissions = permissions
    }

    static clearUserPermissions() {
        Eval.userPermissions = ""
    }

    static userHasPermission(permission: string): boolean {
        if (Eval.userPermissions) {
            return Eval.userPermissions.indexOf(permission) !== -1
        }
        return false
    }

    static getCurrentTopicValue(topic: string): any {
        return MB.getCurrentData(topic)
    }

    static publishTopic(topic: string, value: any) {
        MB.publish(topic, value)
    }

    static isConditionTrue(componentId: string, code: string, value: string = ""): boolean { 
        let compiledCode: any 
        let result: any

        // Compile the code
        try {
            compiledCode = compileCode(code)
        } catch (error) {
            throw new Error(`Compilation of JavaScript failed for component "${componentId}".<br/><b>${code}</b><br/>${ErrorUtils.cvt(error)}`)
        }
        
        // Run the code
        try {
            result = compiledCode({
                value: value,
                console: console, 
                isProduction: Eval.isProduction, 
                hasMoreThanOneAt: Eval.hasMoreThanOneAt, 
                userHasPermission: Eval.userHasPermission,
                getCurrentTopicValue: Eval.getCurrentTopicValue,
                publishTopic: Eval.publishTopic })
        } catch (error) {
            throw new Error(`Exception occurred while running JavaScript for component "${componentId}".<br/><b>${code}</b><br/>${ErrorUtils.cvt(error)}`)

        }
        
        // Check result is a boolean
        if (typeof (result) !== "boolean") {
            throw new Error(`Error evaluating JavaScript for component "${componentId}".<br/><b>${code}</b><br/>Result must be true or false. Was ${typeof (result)}: ${result}`)
        }

        return result
    }
}

/**
 * Sandbox code to compile JavaScript ready for execution. Ensures that the code only has access to objects and methods 
 * that are passed into the sandbox. Uses 'with' which is deprecated, so might need an alternative. For more details see:
 * https://blog.risingstack.com/writing-a-javascript-framework-sandboxed-code-evaluation/
 */

const sandboxProxies = new WeakMap()

function compileCode(src: string) {
    src = 'with (sandbox) {' + src + '}'
    const code = new Function('sandbox', src)

    return function (sandbox: any) {
        if (!sandboxProxies.has(sandbox)) {
            const sandboxProxy = new Proxy(sandbox, { has, get })
            sandboxProxies.set(sandbox, sandboxProxy)
        }
        return code(sandboxProxies.get(sandbox))
    }
}

function has(target: any, key: any) {
    return true
}

function get(target: any, key: any) {
    if (key === Symbol.unscopables) return undefined
    return target[key]
}